import styled from 'styled-components';
import { TextField, Button } from '@material-ui/core';

export const Container = styled.div`
    background-color: orange;
    display: flex;
    flex-direction: row;
    min-height: 100vh;
    height: 100%;
`

export const Label = styled.label`
    text-align: center;
`

export const Card = styled.div`
    background-color: white;
    display: flex;
    flex-direction: column;
    margin-left: auto;
    margin-right: auto;
    width: 80%;
`

export const Title = styled.h1`
    text-align: center;
`
export const Input = styled(TextField)`

`

export const ShortUrl = styled.p`
    text-align: center;
`

export const UrlButton = styled(Button)`

`

export const Form = styled.form`
    justify-content: center;

`