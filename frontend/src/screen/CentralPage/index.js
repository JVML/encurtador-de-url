import React, { useState } from 'react'
import { Container, Card, Title, Label, Input, ShortUrl, UrlButton, Form } from './style'
import { urlShortener } from '../../services/requests'


const CentralPage = () => {
    const [inputUrl, setInputUrl] = useState('');
    const [shortenedUrl, setShortenedUrl] = useState('');
    const handleSubmit = () => {
        urlShortener(inputUrl).then(response => response.json()).then(console.log);
    }

    return (
        <Container>
            <Card>
                <Title>
                    { inputUrl} Url Shortener
                </Title>
                <Form onSubmit={handleSubmit}>
                    <Label>
                        Insira a Url que você desejar
                    </Label>
                    <Input
                        label='url'
                        onChange={event => setInputUrl(event.target.value)}
                    />
                    <ShortUrl>
                        {shortenedUrl}   
                    </ShortUrl>
                    <UrlButton onClick={handleSubmit}>
                        Teste
                    </UrlButton>
                </Form>
            </Card>
        </Container>
    )
}

export default CentralPage