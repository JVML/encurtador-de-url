export const ENDPOINTS = {
    API: {
        base_url: `http://localhost:8000`,
        urlShortener: {
            route: '/shortUrl',
            method: 'POST',
        }
    }
}