import { ENDPOINTS } from '../settings/endpoints';

const common_header = {
  "Content-Type": "application/json"
}

export const urlShortener = async (url) => {
    const urlShortener = ENDPOINTS.API.base_url + ENDPOINTS.API.urlShortener.route;
    console.log({ "url": url });
    return new Promise((resolve, reject) => {
        fetch(urlShortener, {
            header: common_header,
            method: ENDPOINTS.API.urlShortener.method,
            body: JSON.stringify({
                url: url,
        })
        })
        .then(resolve)
        .catch(reject)
    })
}

export const redirectUrl = async (id) => {
    const url = ENDPOINTS.API.base_url + id;
    return new Promise((resolve, reject) => {
        fetch(url).then(resolve).then(reject);
    })
}

export const ranking = async () => {
    const url = ENDPOINTS.API.base_url + '/ranking';
    return new Promise((resolve, reject) => {
        fetch(url).then(resolve).then(reject);
    })
}