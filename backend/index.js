// var express = require('express')
// var app = express()

// app.get('/', function (req, res) {
//   res.send('Hello World!')
// })

// app.listen(8000, function () {
//   console.log('Example app listening on port 8000!')
// })

const express = require('express');
const http = require('http');
const cors = require('cors');
const morgan = require('morgan');
const router = require('./routes/urlRoutes');
const databaseHelper = require('./database');

const app = express();
const httpServer = http.createServer(app);

databaseHelper.connect();

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(router);

const port = process.env.PORT || 8000;

httpServer.listen(8000, function () {
  console.log('Listening on port 8000!')
})