const express = require('express');
const validator = require('validator');

const UrlModel = require('../models/urlModel');

const router = new express.Router();

router.post('/shortUrl', async (req, res) => {
    try {
        const { url } = req.body;
        if (!url) {
            return res.status(400).send({ message: 'empty url' });
        }

        if (!validator.isURL(url, { require_protocol: true })) {
            return res.status(400).send({ message: 'invalid url' });
        }

        const exists = await UrlModel.findOne({ url });
        if (exists) {
            const updatedCount = await UrlModel.findOneAndUpdate({ url }, { $inc: { "count": 1 } });
            return res.status(200).send(`http://localhost:8000/${updatedCount._id}`);
        }

        const newUrlModel = await UrlModel.create({
            url,
        })

        const response = await UrlModel.findOne({ url });
        
        return res.status(200).send(`http://localhost:8000/${response._id}`)
    } catch (error) {
        console.log(error);
        return res.status(400).send(error)
    }
})

router.get('/ranking', async (req, res) => {
    try {
        const list = await UrlModel.find().sort({'count':'desc'}).limit(5);
        return res.status(200).send(list);

    } catch (error) {
        console.log(error);
        return res.status(400).send(error)
    }
})

router.get('/:id', async (req, res) => {
    try {
        const originalObject = await UrlModel.findById(req.params.id);
        const { url } = originalObject;
        return res.redirect(url);
    } catch (error) {
        console.log(error);
        return res.status(400).send(error)
    }
})


module.exports = router;