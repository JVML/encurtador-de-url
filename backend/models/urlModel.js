const mongoose = require('mongoose');
const shortid = require('shortid');

const UrlSchema = new mongoose.Schema({
      url: {
            type: String,
            unique: true
      },
      count: {
            type: Number,     
            default: 1
      },
      _id: {
            type: String,
            default: shortid.generate,
            unique: true
      },
})

const UrlModel = mongoose.model('Url', UrlSchema);

module.exports = UrlModel;